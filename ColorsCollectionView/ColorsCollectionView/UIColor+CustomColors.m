//
//  UIColor+CustomColors.m
//  ColorsCollectionView
//
//  Created by Mac User on 7/3/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+(NSArray *)arrayOfColors
{
    return @[[UIColor redColor],
             [UIColor greenColor],
             [UIColor blueColor],
             [UIColor yellowColor]
             ];
}

+(UIColor *)colorAtIndex:(NSInteger)index
{
    return [self arrayOfColors][index];
}

@end
