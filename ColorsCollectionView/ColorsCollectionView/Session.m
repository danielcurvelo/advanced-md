//
//  Session.m
//  
//
//  Created by Mac User on 7/5/15.
//
//

#import "Session.h"

static NSString * const rowsNumberKey = @"rowsNumberKey";
static NSString * const columnsNumberKey = @"columnsNumberKey";
static NSString * const colorsSelectedKey = @"colorsSelectedKey";

@implementation Session

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self) {
        _numberOfRows = [aDecoder decodeIntegerForKey:rowsNumberKey];
        _numberOfColumns = [aDecoder decodeIntegerForKey:columnsNumberKey];
        _colorsSelected = [aDecoder decodeObjectForKey:colorsSelectedKey];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeInteger:self.numberOfRows forKey:rowsNumberKey];
    [aCoder encodeInteger:self.numberOfColumns forKey:columnsNumberKey];
    [aCoder encodeObject:self.colorsSelected forKey:colorsSelectedKey];
}

@end
