//
//  ViewController.m
//  ColorsCollectionView
//
//  Created by Mac User on 7/3/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "ViewController.h"
#import "ColorsCollectionViewController.h"
#import "UIColor+CustomColors.h"
#import "ColorsCollectionViewDataSourceAndDelegate.h"
#import "Session.h"

static NSString * const colorSelectedRestorationKey = @"colorsSelectedRestorationKey";
static NSString * const rowsTextRestorationKey = @"rowsRestorationKey";
static NSString * const columnTextRestorationKey = @"columnRestorationKey";

@interface ViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *rowsTextField;
@property (strong, nonatomic) IBOutlet UITextField *columnTextField;
@property (strong, nonatomic) IBOutlet UIButton *colorBtn0;
@property (strong, nonatomic) IBOutlet UIButton *colorBtn1;
@property (strong, nonatomic) IBOutlet UIButton *colorBtn2;
@property (strong, nonatomic) IBOutlet UIButton *colorBtn3;
@property (retain, nonatomic) NSMutableArray * colorsSelected;
@property (copy, nonatomic) NSArray *arrayOfBtns;

@end

@implementation ViewController

#pragma mark -
#pragma mark - View's LifeCycle.

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.colorsSelected = [NSMutableArray new];
    
    //Mark: Setting up the buttons by using a for loop and helper method
    self.arrayOfBtns = @[self.colorBtn0,self.colorBtn1,self.colorBtn2,self.colorBtn3];
    for (UIButton *button in self.arrayOfBtns) {
        [self configureBtn:button withColor:[UIColor colorAtIndex:button.tag]];
    }
}

#pragma mark -
#pragma mark - Color Selection Buttons Actions and Setup methods.

- (void)configureBtn:(UIButton *)button withColor:(UIColor *)color
{

    [button.layer setCornerRadius:button.frame.size.width/2];
    [button.layer setBorderColor:[UIColor grayColor].CGColor];
    [button.layer setBorderWidth:1];
    [button setBackgroundColor:color];
    
}

- (IBAction)ColorSelectionButtonSelected:(UIButton *)btn {
    [btn setSelected:!btn.selected]; // Buttons switch states when pressed
    
    if (btn.selected) {
        [self.colorsSelected addObject:[NSNumber numberWithInteger:btn.tag]]; // Adding the tag/index to the array of selected colors
    }
    else
    {
        [self.colorsSelected removeObject:[NSNumber numberWithInteger:btn.tag]]; // Removing the tag/index to the array of selected colors.
    }
}

#pragma mark -
#pragma mark - Create Button Action

- (IBAction)CreatePatternButtonPressed:(id)sender {
    Session * session = [Session new]; // Instantiating a new Session object

    if (_colorsSelected) { // Checking to see if any color has been selected.
        //MARK: If there are colors selected then perform the segue with a full object.
        session.numberOfRows = [self.rowsTextField.text integerValue];
        session.numberOfColumns = [self.columnTextField.text integerValue];
        session.colorsSelected = [_colorsSelected copy];
        
        //MARK: We're saving the session in NSUserDefaults using NSCoder in order to retrieve it in restoration.
        NSData *sessionData = [NSKeyedArchiver archivedDataWithRootObject:session];
        [[NSUserDefaults standardUserDefaults] setObject:sessionData forKey:sessionCoderKey];
        
        [self performSegueWithIdentifier:@"toCollectionView" sender:session];
    }
    else // Otherwise just send an empty session.
    {
        session.numberOfColumns = 0;
        session.numberOfRows = 0;
        [self performSegueWithIdentifier:@"toCollectionView" sender:session];
    }
}

#pragma mark -
#pragma mark - Dismiss Keyboard Action

- (IBAction)dismissKeyboard:(id)sender { // If user taps on the screen while the keyboard is up, dismiss it.
    
    [self.rowsTextField resignFirstResponder];
    [self.columnTextField resignFirstResponder];
    
}

#pragma mark -
#pragma mark - Prepare for Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"toCollectionView"]) {
        ColorsCollectionViewController *colorsViewController =(ColorsCollectionViewController *)[segue destinationViewController];
        [colorsViewController setSession:(Session *)sender]; // Send the session and assign it to the ColorsCollectionViewController.
    }
}

#pragma mark -
#pragma mark - Restoration State

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    //MARK: Encoding the fields and states that we want to preserve for restoration.
    
    [coder encodeObject:self.rowsTextField.text forKey:rowsTextRestorationKey];
    [coder encodeObject:self.columnTextField.text forKey:columnTextRestorationKey];
    [coder encodeObject:self.colorsSelected forKey:colorSelectedRestorationKey];
    
    for (int i = 0; i < self.arrayOfBtns.count ; i++) {
        UIButton *button = [self.arrayOfBtns objectAtIndex:i];
        [coder encodeBool:button.selected forKey:[NSString stringWithFormat:@"button%dKey",i]];
    }
    
    [super encodeRestorableStateWithCoder:coder];
    // We need to call super to make sure that the parent view controller has time to save state.
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    
    //MARK: Decoding the fields and states that we want to restore and setting them to their respective properties.

    self.rowsTextField.text = [coder decodeObjectForKey:rowsTextRestorationKey];
    self.columnTextField.text = [coder decodeObjectForKey:columnTextRestorationKey];
    self.colorsSelected = [coder decodeObjectForKey:colorSelectedRestorationKey];
    
    for (int i = 0; i < self.arrayOfBtns.count ; i++) {
        UIButton *button = [self.arrayOfBtns objectAtIndex:i];
        button.selected = [coder decodeBoolForKey:[NSString stringWithFormat:@"button%dKey",i]];
         }
    
    [super decodeRestorableStateWithCoder:coder];
    // We need to call super to make sure that the parent view controller has time to restore state.
}


@end
