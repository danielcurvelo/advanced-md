//
//  ViewController.h
//  ColorsCollectionView
//
//  Created by Mac User on 7/3/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <UIKit/UIKit.h>

// =============================================================================
// Our ViewController Class is going to get all of the user input and send the
// data to the ColorsCollectionViewController to be used and handled properly.
// =============================================================================


@interface ViewController : UIViewController

@end

