//
//  ColorCollectionViewCell.m
//  ColorsCollectionView
//
//  Created by Mac User on 7/7/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "ColorCollectionViewCell.h"

@implementation ColorCollectionViewCell

-(void)layoutSubviews
{
    [super layoutSubviews];
        self.layer.cornerRadius = self.frame.size.width / 2; //Setting our Corner Radius to achieve the circular shape.
}

+(NSString *)reuseIdentifier
{
    return @"colorCell"; //Cell's Identifier.
}

@end
