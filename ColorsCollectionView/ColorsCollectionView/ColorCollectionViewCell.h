//
//  ColorCollectionViewCell.h
//  ColorsCollectionView
//
//  Created by Mac User on 7/7/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorCollectionViewCell : UICollectionViewCell

+(NSString *)reuseIdentifier;

@end
