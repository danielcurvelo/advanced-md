//
//  ColorsCollectionViewController.h
//  ColorsCollectionView
//
//  Created by Mac User on 7/3/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session.h"

// =============================================================================
// Base class for the CollectionView of colored cells. This Class will take care
// of controlling anything that has to do with the views.
//=============================================================================

static NSString * const reuseIdentifier = @"cell";

@interface ColorsCollectionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) Session *session;

@end
