//
//  ColorsCollectionViewDataSourceAndDelegate.m
//  ColorsCollectionView
//
//  Created by Mac User on 7/7/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "ColorsCollectionViewDataSourceAndDelegate.h"
#import "UIColor+CustomColors.h"
#import "ColorCollectionViewCell.h"
#import "Session.h"

@interface ColorsCollectionViewDataSourceAndDelegate ()

@property (nonatomic, assign) NSInteger colorIndex;

@end

@implementation ColorsCollectionViewDataSourceAndDelegate

#pragma mark -
#pragma mark DataSource Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //MARK: Check first if we have any session available in this class. If not (usually after an restoration), unarchive the last session stored in NSUserDefaults

    if (!self.session) {
        NSData *sessionData = [[NSUserDefaults standardUserDefaults] objectForKey:sessionCoderKey];
        self.session = [NSKeyedUnarchiver unarchiveObjectWithData:sessionData];
        }
    
    self.colorIndex = 0; // Reseting color index so we can start at index 0 everytime.
    return self.session.numberOfRows * self.session.numberOfColumns; // Once we have the session then use it's properties to calculate the number of items per section.
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // MARK: Registering the collection view cell into the collection view.
    [collectionView registerClass:[ColorCollectionViewCell class] forCellWithReuseIdentifier:[ColorCollectionViewCell reuseIdentifier]];

    ColorCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:[ColorCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
    
    [self configureCell:cell]; //Pass in the cell to be configured
    return cell;
}

-(void)configureCell:(ColorCollectionViewCell *)cell
{
    //MARK: In this method we'll use the cell that has been passed and use the colorIndex to parse through our array of colors.
    
    NSNumber * indexOfColor =(NSNumber *)[self.session.colorsSelected objectAtIndex:self.colorIndex];
    UIColor *backGroundColor = [UIColor colorAtIndex:[indexOfColor integerValue]];
    
    [cell setBackgroundColor:backGroundColor];
    
    self.colorIndex = (self.colorIndex < self.session.colorsSelected.count -1) ? self.colorIndex + 1 : 0;
    // We increase and reset to zero our colorIndex as many times as necessary in order to achieve the expected pattern.
}

#pragma mark -
#pragma mark Flow Delegate Methods

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //MARK: Setting the right size for our items.
    CGFloat cellSize = collectionView.frame.size.width/self.session.numberOfColumns;
    return CGSizeMake(cellSize, cellSize);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0; //We want to get rid of any space between our cells.
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0); // We want to get rid of any inset that the collection view may have.
}

@end
