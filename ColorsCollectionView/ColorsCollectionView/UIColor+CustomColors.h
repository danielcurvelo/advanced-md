//
//  UIColor+CustomColors.h
//  ColorsCollectionView
//
//  Created by Mac User on 7/3/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <UIKit/UIKit.h>

// =============================================================================
// Color Category allows us flexibily of transportation of our array of colors
// and the new class method colorAtIndex:index allows us to easily get the right
// color for each index.
// =============================================================================

@interface UIColor (CustomColors)

+(NSArray *)arrayOfColors;

+(UIColor *)colorAtIndex:(NSInteger)index;

@end
