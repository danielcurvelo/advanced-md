//
//  Session.h
//  
//
//  Created by Mac User on 7/5/15.
//
//

#import <Foundation/Foundation.h>

// =============================================================================
// Model Object that temporarily stores the information about the session.
// =============================================================================

@interface Session : NSObject <NSCoding>

@property (nonatomic,assign) NSInteger numberOfRows;
@property (nonatomic,assign) NSInteger numberOfColumns;
@property (nonatomic,copy) NSArray *colorsSelected;

@end
