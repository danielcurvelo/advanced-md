//
//  ColorsCollectionViewController.m
//  ColorsCollectionView
//
//  Created by Mac User on 7/3/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import "ColorsCollectionViewController.h"
#import "ColorsCollectionViewDataSourceAndDelegate.h"

@interface ColorsCollectionViewController ()

@property (strong, nonatomic) IBOutlet ColorsCollectionViewDataSourceAndDelegate *dataSourceDelegate;

@end

@implementation ColorsCollectionViewController

#pragma mark -
#pragma mark - View's LifeCycle

-(void)viewWillAppear:(BOOL)animated
{
    // We pass the session to the dataSource and Delegate. IMPORTANT: We pass it only if it's not nil.
    
    if (self.session) {
        self.dataSourceDelegate.session = self.session;
    }
    [super viewWillAppear:animated];
}

#pragma mark -
#pragma mark - Orientation Transition

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
     //MARK: Invalidating Layout during orientation changes.
                  
         [self.collectionView.collectionViewLayout invalidateLayout]; // It resets the layout. This way it will redraw the cells with their respective sizes.

     } completion:nil];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

#pragma mark -
#pragma mark - Take ScreenShot

- (IBAction)saveScreenShot:(id)sender {
    
    // Define the dimensions of the screenshot you want to take (the entire screen in this case)
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *sourceImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //now we will position the image, X/Y away from top left corner to get the portion we want
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
    [sourceImage drawAtPoint:CGPointMake(0, 0)];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(croppedImage,nil, nil, nil);
    
}



@end
