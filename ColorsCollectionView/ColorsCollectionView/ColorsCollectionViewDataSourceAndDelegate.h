//
//  ColorsCollectionViewDataSourceAndDelegate.h
//  ColorsCollectionView
//
//  Created by Mac User on 7/7/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;
@class Session;

// =============================================================================
// This class will hold the DataSource and the Flow Layout Delegate for our
// collection view. It's in charge of handling the data and events. 
//=============================================================================

static NSString * const sessionCoderKey = @"sessionCoderKey";

@interface ColorsCollectionViewDataSourceAndDelegate : NSObject <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) Session * session;

@end
