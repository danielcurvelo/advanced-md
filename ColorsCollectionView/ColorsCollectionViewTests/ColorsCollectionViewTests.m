//
//  ColorsCollectionViewTests.m
//  ColorsCollectionViewTests
//
//  Created by Mac User on 7/3/15.
//  Copyright (c) 2015 DanielCurvelo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ColorsCollectionViewDataSourceAndDelegate.h"
#import "ColorsCollectionViewController.h"
#import "ColorCollectionViewCell.h"

@interface ColorsCollectionViewTests : XCTestCase

@property (nonatomic,strong) ColorsCollectionViewDataSourceAndDelegate *dataSourceAndDelegate;
@property (nonatomic,strong) ColorsCollectionViewController *colorsCollectionViewController;

@end

@implementation ColorsCollectionViewTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.colorsCollectionViewController = [ColorsCollectionViewController new];
    self.dataSourceAndDelegate = [ColorsCollectionViewDataSourceAndDelegate new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.colorsCollectionViewController = nil;
    self.dataSourceAndDelegate = nil;

    [super tearDown];
}

#pragma mark -
#pragma mark - Testing the Datasource protocol and connection

- (void)testThatDataSourceAndDelegateConformsToUICollectionViewDataSource
{
    BOOL itConforms = [self.dataSourceAndDelegate conformsToProtocol:@protocol(UICollectionViewDataSource)];
    XCTAssertTrue(itConforms);
}

- (void)testThatCollectionViewDataSourceIsConnected
{
    if (self.dataSourceAndDelegate.session) {
        XCTAssertNotNil(self.colorsCollectionViewController.collectionView.dataSource, @"DataSource should be connected");
    }
    else
    {
        XCTAssertNil(self.colorsCollectionViewController.collectionView.dataSource, @"DataSource should not be connected");
    }
}

#pragma mark -
#pragma mark - Testing the Datasource required Methods

- (void)testCollectionViewNumberOfItemsInSection
{
    int expectedNumberOfItems = 3;
    
    if (self.dataSourceAndDelegate.session) {
        XCTAssertEqual([self.colorsCollectionViewController.collectionView numberOfItemsInSection:0], expectedNumberOfItems);
    }
    else
    {
        XCTAssertNotEqual([self.colorsCollectionViewController.collectionView numberOfItemsInSection:0], expectedNumberOfItems);
    }
}

- (void)testCollectionViewCellForItemAtIndexPathReturnsCellClass
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    UICollectionViewCell *cell = [self.colorsCollectionViewController.collectionView cellForItemAtIndexPath:indexPath];
    
    if (self.dataSourceAndDelegate.session) {
    XCTAssertEqual([cell class],[ColorCollectionViewCell class]);
    }
}

#pragma mark -
#pragma mark - Testing the Flow Layout Delegate protocol and connections

- (void)testThatDataSourceAndDelegateConformsToUICollectionViewDelegateFlowLayout
{
    BOOL itConforms = [self.dataSourceAndDelegate conformsToProtocol:@protocol(UICollectionViewDelegateFlowLayout)];
    XCTAssertTrue(itConforms);
}

- (void)testCollectionViewDelegateIsConnected
{
    if (self.dataSourceAndDelegate.session) {
        XCTAssertNotNil(self.colorsCollectionViewController.collectionView.delegate, @"Delegate should be connected");
    }
    else
    {
        XCTAssertNil(self.colorsCollectionViewController.collectionView.delegate, @"Delegate should not be connected");
    }
}

@end
